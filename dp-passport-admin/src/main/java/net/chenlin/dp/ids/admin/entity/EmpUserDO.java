package net.chenlin.dp.ids.admin.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * passport用户DO
 * @author zhouchenglin[yczclcn@163.com]
 */
public class EmpUserDO implements Serializable {

    private Long id;

    private String username;

    private String salt;

    private String password;

    private Integer status;

    private Date gmtLastLogin;

    public EmpUserDO() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGmtLastLogin() {
        return gmtLastLogin;
    }

    public void setGmtLastLogin(Date gmtLastLogin) {
        this.gmtLastLogin = gmtLastLogin;
    }

    @Override
    public String toString() {
        return "EmpUserDO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", salt='" + salt + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", gmtLastLogin=" + gmtLastLogin +
                '}';
    }

}
