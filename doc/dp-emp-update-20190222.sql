SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `passport_config`
-- ----------------------------
DROP TABLE IF EXISTS `passport_config`;
CREATE TABLE `passport_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `macro_key` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数编码',
  `macro_value` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数值',
  `remark` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数描述',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='单点登录系统参数配置';

-- ----------------------------
--  Records of `passport_config`
-- ----------------------------
BEGIN;
INSERT INTO `passport_config` VALUES ('1', 'ids:server:public:key', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCMnaCxdqh0zGdrogP9PYAn3tZChl423YtEkVtyD5qiWfEP50+AgsNjuHH6iknIzeXKI7m0JcGNmbzClpPpX+BOct+Q7kvrnZ6WtqOPQWB1PKILJ99yzuojFFutbMS/7b2ofo3+7F7wOvbApWywDa6f1P7vB8hoEwD0PK3Lr2DegwIDAQAB', 'RSA公钥', null, null, '2019-02-22 15:16:21', '2019-02-22 15:16:29'), ('2', 'ids:server:private:key', 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIydoLF2qHTMZ2uiA/09gCfe1kKGXjbdi0SRW3IPmqJZ8Q/nT4CCw2O4cfqKScjN5cojubQlwY2ZvMKWk+lf4E5y35DuS+udnpa2o49BYHU8ogsn33LO6iMUW61sxL/tvah+jf7sXvA69sClbLANrp/U/u8HyGgTAPQ8rcuvYN6DAgMBAAECgYB6m5baJlSMQw6KUyKXVWDBYxP/vZm4zUP57aSGuARlO3duHoq7y0kDUnhPtPTRGnca4juyUJmMFlj4csNV4jt52iPs7Hy1aQdoDKM3BGdmT0dN6cNSg7Yf8nI/pmTV6qTsHa31SPjndsDK1QQOlVFOPSjgfFFlCcaiyLNvgGCqgQJBAOgVlfUICSZP5LrwH3RydsDf2f1Mg+mhayvT+B4x9bvFZlJppgibyVpnmxOZvf+fjp/NonMb3Q/AOIrKkN+9A9sCQQCbGxa/TkT/lSaDskYDe/D6WoWG9kvd+FCCagYfI09yrssjdac9OaugnuiwQaw73KlH3mnWkvKcIHUYLk/nQeR5AkEAxZa7GOZCg++5GL6eyRE6sXLjsJeYJyxahfHtaCe4wqiJONjhlqqrfuB+Uu2tqRg64Sq9h6rNRj+s490aAqJ1yQJBAJOetomgzp6Jr7xL4cLTJUjA+rC2nQJHoSheDDF6g9Tcc03uXhsxVh1q4H9QIcJfg2V5UzX7aZgadBdtjHLHDdECQDHlSrrs79RJ7VbbjNffhLiR/k3rON5qI55wZ5GlNAA5rQzLpiKtE8k6mTSsSahsKq8EwWn6WkcVLxT/6T/GYv0=', 'RSA私钥', null, null, '2019-02-22 15:16:25', '2019-02-22 15:16:31');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
