/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : 127.0.0.1
 Source Database       : dp-emp

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : utf-8

 Date: 09/27/2018 11:09:51 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `emp_user`
-- ----------------------------
DROP TABLE IF EXISTS `emp_user`;
CREATE TABLE `emp_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名',
  `salt` varchar(20) DEFAULT NULL COMMENT '密码salt',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `status` tinyint(4) DEFAULT NULL COMMENT '用户状态，1：正常，0：禁用',
  `gmt_last_login` datetime DEFAULT NULL COMMENT '上次登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `emp_user`
-- ----------------------------
BEGIN;
INSERT INTO `emp_user` VALUES ('1', 'admin', 'tozxr7', 'EDA8DBC10391E81C37669CCFBDCACF0F', '1', '2018-09-27 11:06:49');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
